import { Component } from '@angular/core';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html'
})
export class GlobalComponent {

  url: string = 'http://localhost:8000/api/';
 
}
