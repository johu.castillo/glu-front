import { Component, OnInit } from '@angular/core';

import { UsuariosService } from '../../../services/usuarios.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html'
})
export class UsuariosComponent implements OnInit {
  usuarios: any;
  cantidad_usuarios: any;

  constructor(private usuarioservice:UsuariosService) { }

  ngOnInit() {
    this.ObtenerUsuarios();

  }

  ObtenerUsuarios() {
    this.usuarioservice.obtener_usuarios().subscribe(
      data => { this.usuarios = data},
      err => console.error(err),      () => console.log(this.usuarios)
  );}

  ContarUsuarios() {
  this.usuarioservice.contar_usuarios().subscribe(
    data => { this.cantidad_usuarios = data},
    err => console.error(err),      () => console.log(this.cantidad_usuarios)
  );}

  BorrarUsuarios(id) {
    this.usuarioservice.borrar_usuario(id).subscribe(
      data => { this.usuarios = data,this.ngOnInit()},
      err => console.error(err),      () => console.log(this.usuarios)
  );}
  

}
