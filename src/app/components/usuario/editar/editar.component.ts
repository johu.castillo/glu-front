import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { UsuariosService } from '../../../services/usuarios.service';


@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  public usuario:any ={
    name:"", primer_nombre: "", apellido: "", email: "", password: "",
  };
  public myForm: FormGroup;
  public id;
  

  constructor(private _ar: ActivatedRoute, private _fb: FormBuilder, private router:Router,private usuarioservice:UsuariosService) { }

  ngOnInit() {
    this.id=this._ar.snapshot.params["id"];
    this.ObUsuarios(this.id);

    this.myForm = this._fb.group({
      name: [''],
      primer_nombre: [''],
      apellido: [''],
      email: [''],
      password: [''],
    });
  }

  save(model) {
    this.editar_usuario(model);
  }

  editar_usuario(formulario) {
    this.usuarioservice.editar_usuario(formulario,this.id).subscribe(
       data => {
         this.usuario = data;
        this.router.navigate(['/usuarios']);
      },
       error => {
         console.error("Error guardando!");
         return Observable.throw(error);
       }
    );
  }

  ObUsuarios(id) {
    this.usuarioservice.ob_usuario(id).subscribe(
      data => { this.usuario = data},
      err => console.error(err),      () => console.log(this.usuario)
  );}

}
