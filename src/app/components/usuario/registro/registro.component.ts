import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { UsuariosService } from '../../../services/usuarios.service';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  public usuario;
  public myForm: FormGroup;

  constructor(private notificacion: ToastrService,private _fb: FormBuilder, private router:Router,private usuarioservice:UsuariosService) { }

  ngOnInit() {

    this.myForm = this._fb.group({
      name: [''],
      primer_nombre: [''],
      apellido: [''],
      email: [''],
      password: [''],
    });

  }
  save(model) {
    this.crear_usuario(model);
  }

  crear_usuario(formulario) {
    this.usuarioservice.crear_usuario(formulario).subscribe(
       data => {
        this.usuario = data;
        this.notificacion.success('Usuario creado con Exito.')//para la notificacion
        this.router.navigate(['/usuarios']);
      },
       error => {
         console.error("Error guardando!");
         return Observable.throw(error);
       }
    );
  }
}
