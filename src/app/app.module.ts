import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { GlobalComponent } from './components/global/global.component';
import { SidepanelComponent } from './components/template/sidepanel/sidepanel.component';
import { NavbarComponent } from './components/template/navbar/navbar.component';
import { UsuariosService } from './services/usuarios.service';

//http
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './routes/app-routing.module';
import { PrincipalComponent } from './components/principal/principal.component';
import { UsuariosComponent } from './components/usuario/usuarios/usuarios.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { RegistroComponent } from './components/usuario/registro/registro.component';
import { EditarComponent } from './components/usuario/editar/editar.component';

@NgModule({
  declarations: [
    AppComponent,
    SidepanelComponent,
    GlobalComponent,
    PrincipalComponent,
    UsuariosComponent,
    NavbarComponent,
    FooterComponent,
    RegistroComponent,
    EditarComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [GlobalComponent,UsuariosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
