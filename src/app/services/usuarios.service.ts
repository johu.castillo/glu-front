import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
 //http
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalComponent } from '../components/global/global.component' ;

const httpOptions = {
  method: 'POST',
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()

export class UsuariosService {

  constructor(private http:HttpClient,private global: GlobalComponent) { }

  obtener_usuarios():Observable<any> {
    return this.http.get(this.global.url+'ObtenerUsuarios')
  }


  contar_usuarios():Observable<any> {
    return this.http.get(this.global.url+'ContarUsuarios')
  }

  crear_usuario(formulario) {
   return this.http.post(this.global.url+'CrearUsuario', formulario, httpOptions);
  }

  editar_usuario(formulario,id) {
    return this.http.post(this.global.url+'EditarUsuario/'+id, formulario, httpOptions);
   }

  borrar_usuario(id_usuario):Observable<any>{
    return this.http.get(this.global.url+'BorrarUsuario/'+id_usuario)
  }

  ob_usuario(id_usuario):Observable<any>{
    return this.http.get(this.global.url+'ObtenerUsuario/'+id_usuario)
  }



}
