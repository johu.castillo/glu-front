import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

//componentes
import { PrincipalComponent } from '../components/principal/principal.component';
import { UsuariosComponent } from '../components/usuario/usuarios/usuarios.component';
import { RegistroComponent } from '../components/usuario/registro/registro.component';
import { EditarComponent } from '../components/usuario/editar/editar.component';

const routes: Routes = [
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },
  { path: 'inicio', component: PrincipalComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'registro_usuarios', component: RegistroComponent },
  { path: 'editar_usuarios/:id', component: EditarComponent },
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
